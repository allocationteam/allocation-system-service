/**
 * @author Mayur Shah
 * @description This controller is used for perform crud operation using filtering as well.
*/

let models = require('../models');
const Sequelize = models.Sequelize;
const Op = models.Sequelize.Op;

//This controller is used for getting all accounts details.
exports.getUserAndRoles = function (req, res) {
    models.UserRoles.findAll({
        where: {
            $or: {
                user_email_id: {
                    $like: "%" + req.query.userEmailId + "%"
                }
            },
        },
        attributes: ['user_access_id', 'user_email_id','admin_level', 'po_level', 'customer_level', 'account_level']
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'SUCCESS', data: response });
        }
        else {
            res.send({ status: 'SUCCESS', data: [] });
        }
    })
};

//This controller is used for creating new account.
exports.createNewUserAndRoles = function (req, res) {
    models.UserRoles.findOne({
        where: {
            user_email_id: req.body.user_email_id
        }
    }).then(response => {
        if (response !== null) {
            

            res.send({ status: 'SUCESS', err_msg: 'User access is updated successfully!' });
        } else {
            models.UserRoles.create({
                user_email_id: req.body.user_email_id,
                admin_level: req.body.admin_level,
                po_level: req.body.po_level,
                customer_level: req.body.customer_level,
                account_level: req.body.account_level
            }, {
                    fields: [
                        'user_email_id','admin_level', 'po_level', 'customer_level', 'account_level'
                    ],
                    defaults: {
                        created_at: new Date(),
                        updated_at: new Date()
                    }
                }).then(response => {
                    res.send({ status: 'SUCCESS', message: 'New User with roles is successfully created.' });
                }).catch(Sequelize.ValidationError, function (err) {
                    res.send({ status: 'FAILURE', err_msg: models.sequelizeValidationErrorHandler(err) });
                }).catch(function (err) {
                    res.send({ status: 'FAILURE', err_msg: err });
                });
        }
    })
};