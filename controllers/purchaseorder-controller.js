/**
 * @author Mayur Shah
 * @description This controller is used for perform crud operation.
*/

let models = require('../models');
const Sequelize = models.Sequelize;
const Op = models.Sequelize.Op;

// This controller is used for getting all allocations details.
exports.getAllPurchaseOrders = function (req, res) {
    models.PurchaseOrder.findAll({
        attributes : ['purchase_order_id','po_num','po_description','po_description','status','amount','start_date','end_date'],
        include: [
            {
                model: models.Customer,
                attributes : [
                    'salutation', 'first_name', 'middle_name','last_name'
                ]
            }
        ]
    }).then(function (result) {
        const result_set = result.map(po => {
            return Object.assign(
                {},
                {
                    purchase_order_id: po.purchase_order_id,
                    po_num: po.po_num,
                    po_description: po.po_description,
                    type: po.po_description,
                    status: po.status,
                    amount: po.amount,
                    start_date: po.start_date,
                    end_date: po.end_date,
                    customer_name: po.Customer.salutation + " " + po.Customer.first_name +" "+ po.Customer.middle_name + " " + po.Customer.last_name
                }
            );
        })
        res.json({ status: 'SUCCESS', data: result_set });
    }, function (err) {
        console.log("error occured", err);
        res.json({ status: 'FAILED', msg_code: 'EXP', message: err });
    });
};


//This controller is used for creating new purchase order.
exports.createNewPurchaseOrder = function (req, res) {
    models.PurchaseOrder.findOne({
        where: {
            customer_id: req.body.customer_id,
            po_num: req.body.po_num
        }
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'FAILURE', err_msg: 'Purchase order is already created for particular customer!' });
        } else {
            models.PurchaseOrder.create({
                customer_id: req.body.customer_id,
                po_owner_id: req.body.po_owner_id,
                po_num: req.body.po_num,
                po_description: req.body.po_description,
                type: req.body.type,
                status: req.body.status,
                amount: req.body.amount,
                start_date: req.body.start_date,
                end_date: req.body.end_date
            },{
            fields: [
                'customer_id','po_owner_id', 'po_num', 'po_description', 'type','status','amount', 'start_date', 'end_date'
            ],
            defaults: {
                created_by: 'system',
                updated_by: 'system',
                created_at: new Date(),
                updated_at: new Date()
            }
        }).then(response => {
            res.send({ status: 'SUCCESS', message: 'New Purchase order created successfully .' });
        }).catch(Sequelize.DatabaseError, function (err) {
            if (err.name === 'SequelizeForeignKeyConstraintError') {
                res.send({ status: 'FAILURE', err_msg: 'customer id  is missing in database. Please validate and try again.' });
            } else {
                res.send({ status: 'SUCCESS', err_msg: err });
            }
        }).catch(Sequelize.ValidationError, function (err) {
            res.send({ status: 'FAILURE', err_msg: models.sequelizeValidationErrorHandler(err) });
        }).catch(function(err){
            res.send({status : 'FAILURE', err_msg : err});
        });
    }
 })
};

//This controller is used for deleting new allocation.
exports.deletePurchaseOrder = function (req, res) {
    models.PurchaseOrder.destroy({
        where: {
            purchase_order_id: req.params.purchase_order_id
        }
    }).then(function () {
        models.PurchaseOrder.findAll()
            .then(function (result) {
                res.json({ status: 'SUCCESS', data: result });
            }, function (err) {
                console.log("error occured", err);
                res.json({ status: 'FAILED', msg_code: 'EXP', message: err });
            });
    }, function (err) {
        console.log("error occured", err);
        res.json({ status: 'FAILED', msg_code: 'DELETE_ERROR', message: err });
    });
};