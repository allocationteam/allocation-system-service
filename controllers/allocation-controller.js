/**
 * @author Mayur Shah
 * @description This controller is used for perform crud operation.
*/

let models = require('../models');
const Sequelize = models.Sequelize;
const Op = models.Sequelize.Op;

//This controller is used for getting all allocations details.
exports.getAllAllocations = function (req, res) {
    models.Allocation.findAll()
        .then(function (result) {
            res.json({ status: 'SUCCESS', data: result });
        }, function (err) {
            console.log("error occured", err);
            res.json({ status: 'FAILED', msg_code: 'EXP', message: err });
        });
};



//This controller is used for getting all employee details with filtering using parameter passing from query string.
exports.getSearchedAllocation= function (req, res) {
    models.Allocation.findAll({
        where: {
            $or: {
                allocation_id: req.query.allocationId,
                employee_id : req.query.employeeId
                }
            },
        attributes: ['allocation_id', 'milestone_id', 'employee_id', 'billing_rate', 'allocation_percent', 'start_date', 'end_date'],
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'SUCCESS', data: response });
        }
        else {
            res.send({ status: 'SUCCESS', data: [] });
        }
    })
};


//This controller is used for creating new employee.
exports.createNewAllocation = function (req, res) {
    models.Allocation.findOne({
        where: {
            purchase_order_id: req.body.purchase_order_id,
            employee_id: req.body.employee_id

        }
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'FAILURE', err_msg: 'Allocation is already registered for employee particular purchase order!' });
        } else {
            models.Allocation.create({
                purchase_order_id:req.body.purchase_order_id,
                employee_id: req.body.employee_id,
                role: req.body.role,
                billing_rate: req.body.billing_rate,
                allocation_percent: req.body.allocation_percent,
                start_date: req.body.start_date,
                end_date:req.body.end_date
            },{
            fields: [
                'purchase_order_id','employee_id', 'role', 'billing_rate', 'allocation_percent', 'start_date', 'end_date'
            ],
            defaults: {
                created_by: 'system',
                updated_by: 'system',
                created_at: new Date(),
                updated_at: new Date()
            }
        }).then(response => {
            res.send({ status: 'SUCCESS', message: 'New allocation created successfully .' });
        }).catch(Sequelize.DatabaseError, function (err) {
            if (err.name === 'SequelizeForeignKeyConstraintError') {
                res.send({ status: 'FAILURE', err_msg: 'employeeid or purchase_order_id is missing in database. Please validate and try again.' });
            } else {
                res.send({ status: 'SUCCESS', err_msg: err });
            }
        }).catch(Sequelize.ValidationError, function (err) {
            res.send({ status: 'FAILURE', err_msg: models.sequelizeValidationErrorHandler(err) });
        }).catch(function(err){
            res.send({status : 'FAILURE', err_msg : err});
        });
    }
 })
};


//This controller is used for deleting new allocation.
exports.deleteAllocation = function(req, res) {
    models.Allocation.destroy({
      where: {
        AllocationId: req.params.allocationId
      }
    }).then(function() {
        models.Allocation.findAll()
        .then(function (result) {
            res.json({ status: 'SUCCESS', data: result });
        }, function (err) {
            console.log("error occured", err);
            res.json({ status: 'FAILED', msg_code: 'EXP', message: err });
        });
    },function (err) {
        console.log("error occured", err);
        res.json({ status: 'FAILED', msg_code: 'DELETE_ERROR', message: err });
    });
};



