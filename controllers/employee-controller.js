/**
 * @author Mayur Shah
 * 
 * @description This controller is used for perform crud operation for employee.
*/

let models = require('../models');
const Sequelize = models.Sequelize;
const Op = models.Sequelize.Op;


//This controller is used for getting all employee details without filtering and sorting.
exports.getAllEmployee = function (req, res) {
    models.Employee.findAll()
        .then(function (result) {
            res.json({ status: 'SUCCESS', data: result });
        }, function (err) {
            console.log("error occured", err);
            res.json({ status: 'FAILED', msg_code: 'EXP', message: err });
        });
};


//This controller is used for getting all employee details with filtering using parameter passing from query string.
exports.getSearchedEmployee = function (req, res) {
    models.Employee.findAll({
        include: [
            {
                model: models.Vendor,
                attributes : [
                    'company_name'
                ]
            }
        ],
        where: {
            $or: {
                employee_number: req.query.employeeNumber,
                phone_number : req.query.phoneNumber,
                email_id: {
                    $like: "%" + req.query.emailId + "%"
                },
                first_name: {
                    $like: "%" + req.query.name + "%"
                },
                last_name: {
                    $like: "%" + req.query.name + "%"
                }
                }
            },
        attributes: ['employee_id', 'employee_number', 'salutation', 'first_name', 'middle_name', 'last_name', 'email_id','phone_number','manager_id','vendor_id','status','type'],
    }).then(response => {
        if (response !== null) {
            const result_set = response.map(employee => {
                return Object.assign(
                    {},
                    {
                        employee_id: employee.employee_id,
                        vendor_name: employee.Vendor.company_name,
                        email_id:employee.email_id,
                        phone_number:employee.phone_number,
                        salutation:employee.salutation,
                        first_name: employee.first_name,
                        middle_name:employee.middle_name,
                        last_name:employee.last_name,
                        status:employee.status,
                        type:employee.type
                    } 
                );
            })

            res.send({ status: 'SUCCESS', data: result_set });
        }
        else {
            res.send({ status: 'SUCCESS', data: [] });
        }
    })
};


//This controller is used for creating new employee.
exports.createNewEmployee = function (req, res) {
    models.Employee.findOne({
        where: {
            $and: {
              employee_number: req.body.employee_number,
              first_name: req.body.first_name,
              last_name: req.body.last_name
            }
        }
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'FAILURE', err_msg: 'Employee is already registered!' });
        } else {
            models.Employee.create({
            employee_number:req.body.employee_number,
            salutation: req.body.salutation,
            first_name: req.body.first_name,
            middle_name: req.body.middle_name,
            last_name: req.body.last_name,
            email_id: req.body.email_id,
            phone_number:req.body.phone_number,
            manager_id:req.body.manager_id,
            designation:req.body.designation,
            location:req.body.location,
            status:req.body.status,
            type:req.body.type,
            start_date:req.body.start_date,
            end_date:req.body.end_date,
            vendor_id:req.body.vendor_id
            },{
            fields: [
                'employee_number','salutation', 'first_name', 'middle_name', 'last_name', 'email_id', 'phone_number','manager_id','designation','location','status','type',
                'start_date','end_date','vendor_id'
            ],
            defaults: {
                created_by: 'system',
                updated_by: 'system',
                created_at: new Date(),
                updated_at: new Date()
            }
        }).then(response => {
            res.send({ status: 'SUCCESS', message: 'New employee created successfully .' });
        }).catch(Sequelize.ValidationError, function (err) {
            res.send({ status: 'FAILURE', err_msg: models.sequelizeValidationErrorHandler(err) });
        }).catch(function(err){
            res.send({status : 'FAILURE', err_msg : err});
        });
    }
 })
};

//This controller is used for deleting new allocation.
exports.deleteEmployee = function(req, res) {
    models.Employee.destroy({
      where: {
        employee_id: req.params.employeeId
      }
    }).then(function() {
        models.Employee.findAll()
        .then(function (result) {
            res.json({ status: 'SUCCESS', data: result });
        }, function (err) {
            console.log("error occured", err);
            res.json({ status: 'FAILED', msg_code: 'EXP', message: err });
        });
    },function (err) {
        console.log("error occured", err);
        res.json({ status: 'FAILED', msg_code: 'DELETE_ERROR', message: err });
    });
};