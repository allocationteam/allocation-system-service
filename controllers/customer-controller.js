/**
* @author : Ashish Santikari<ashish.santikari@enquero.com>
* @name : customer-controller
* @description : Controller for customer api
*/

let models = require('../models');
const Sequelize = models.Sequelize;
const Op = models.Sequelize.Op;

exports.getAllCustomers = function (req, res) {
    models.Customer.findAll({
        attributes: ['customer_id', 'location', 'contact_number','salutation','first_name','middle_name','last_name'],
        include: [
            { model: models.Account, attributes:['account_id','account_name']}
          ]
    }) .then(function (result) {
            console.log(result);
            res.json({ status: 'SUCCESS', data: result });
        }, function (err) {
            console.log("error occured", err);
            res.json({ status: 'FAILED', msg_code: 'ALLOC_CUST_3', message: err });
        });
};


//This controller is used for getting all employee details with filtering using parameter passing from query string.
exports.getSearchedCustomer = function (req, res) {
    models.Customer.findAll({
            include: [
                {
                    model: models.Account,
                    attributes : [
                        'account_name'
                    ]
                }
            ],
        where: {
            $or: {
                account_id : req.query.accountId,
                first_name: {
                    $like: "%" + req.query.name + "%"
                },
                last_name: {
                    $like: "%" + req.query.name + "%"
                }
            }
            },
        attributes: ['salutation','customer_id', 'first_name', 'middle_name', 'last_name', 'location'],
    }).then(response => {
        if (response !== null) {
            const result_set = response.map(customer => {
                return Object.assign(
                    {},
                    {
                        customer_id: customer.customer_id,
                        account_name: customer.Account.account_name,
                        location:customer.location,
                        salutation:customer.salutation,
                        first_name: customer.first_name,
                        middle_name:customer.middle_name,
                        last_name:customer.last_name
                    } 
                );
            })

            res.send({ status: 'SUCCESS', data: result_set });
        }
        else {
            res.send({ status: 'SUCCESS', data: [] });
        }
    })
};


//This controller is used for creating new employee.
exports.createNewCustomer = function (req, res) {
    models.Customer.find({
        where: {
            first_name: req.body.first_name,
            account_id: req.body.account_id,
            last_name: req.body.last_name
        }
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'FAILURE', message: 'Customer already exists and is already mapped with the same account' });
        } else {
            models.Customer.create(
                {
                    account_id: req.body.account_id,
                    salutation: req.body.salutation,
                    first_name: req.body.first_name,
                    middle_name: req.body.middle_name,
                    last_name: req.body.last_name,
                    designation: req.body.designation,
                    location: req.body.location,
                    email_id: req.body.email_id,
                    contact_number: req.body.contact_number
                },
                {

                    fields: ['account_id', 'salutation', 'first_name', 'last_name', 'middle_name', 'designation', 'location', 'email_id', 'contact_number'],
                    defaults: {
                        created_by: 'system',
                        updated_by: 'system',
                        created_at: new Date(),
                        updated_at: new Date()
                    }
                }).then(customer => {
                    res.send({ status: 'SUCCESS', message: 'Customer created successfully' });
                }).catch(Sequelize.DatabaseError, function (err) {
                    if (err.name === 'SequelizeForeignKeyConstraintError') {
                        res.send({ status: 'FAILURE', err_msg: 'Account details are missing in database. Please validate and try again.' });
                    } else {
                        res.send({ status: 'SUCCESS', err_msg: err });
                    }
                }).catch(Sequelize.ValidationError, function (err) {
                    res.send({ status: 'FAILURE', err_msg: models.sequelizeValidationErrorHandler(err) });
                }).catch(err => {
                    res.send({ status: 'FAILURE', err_msg: err });
                });
        }
    })
};

