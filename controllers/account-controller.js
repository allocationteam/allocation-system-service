/**
 * @author Mayur Shah
 * 
 * @description This controller is used for perform crud operation also filtering accounts information using required fields.
*/

let models = require('../models');
const Sequelize = models.Sequelize;
const Op = models.Sequelize.Op;

//This controller is used for getting all account names along with account_id;.
exports.getAllAccounts = function (req, res) {
    models.Account.findAll().then(function (result) {
            res.json({ status: 'SUCCESS', data: result });
        }, function (err) {
            console.log("error occured", err);
            res.json({ status: 'FAILED', msg_code: 'ALLOC_03', err_msg: err });
        });
};

//This controller is used for getting searched accounts using filtering parameters accountId,accountName,city
exports.getSearchedAccount = function (req, res) {
    models.Account.findAll({
        where: {
            $or: {
                account_id: req.query.accountId,
                city: req.query.city,
                account_name: {
                    $like: "%" + req.query.accountName + "%"
                }
            },
        },
        attributes: ['account_id', 'account_name', 'address_line_1', 'address_line_2', 'zip_code', 'city', 'country'],
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'SUCCESS', data: response });
        }else {
            res.send({ status: 'SUCCESS', data: [] });
        }
    })
};


//This controller is used for creating new account.
exports.createNewAccount = function (req, res) {
    models.Account.findOne({
        where: {
            account_name: req.body.account_name
        }
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'FAILURE', msg_code:'ALLOC_01',err_msg: 'Account with same name already exists!' });
        } else {
            models.Account.create({
                account_name: req.body.account_name,
                address_line_1: req.body.address_line_1,
                address_line_2: req.body.address_line_2,
                zip_code: req.body.zip_code,
                city: req.body.city,
                country: req.body.country
            }, {
                    fields: [
                        'account_id','account_name', 'address_line_1', 'address_line_2', 'zip_code', 'city', 'country'
                    ],
                    defaults: {
                        created_by: 'system',
                        last_updated_by: 'system',
                        createdAt: new Date(),
                        updatedAt: new Date()
                    }
                }).then(response => {
                    res.send({ status: 'SUCCESS', message: 'New Account created successfully.', err_msg : null});
                }).catch(Sequelize.ValidationError, function (err) {
                    res.send({ status: 'FAILURE',msg_code:'ALLOC_02',err_msg: models.sequelizeValidationErrorHandler(err) });
                }).catch(function (err) {
                    res.send({ status: 'FAILURE',msg_code:'ALLOC_03', err_msg: err });
                });
        }
    })
};