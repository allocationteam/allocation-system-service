/**
 * @author Mayur Shah
 * @description This controller is used for perform crud operation.
*/

let models = require('../models');
const Sequelize = models.Sequelize;
const Op = models.Sequelize.Op;

//This controller is used for getting all allocations details.
exports.getAllMilestones = function (req, res) {
    models.Milestone.findAll()
        .then(function (result) {
            res.json({ status: 'SUCCESS', data: result });
        }, function (err) {
            console.log("error occured", err);
            res.json({ status: 'FAILED', msg_code: 'EXP', message: err });
        });
};

//This controller is used for getting all employee details with filtering using parameter passing from query string.
exports.getSearchedMilestone = function (req, res) {
    models.Milestone.findAll({
        where: {
            $or: {
                milestone_id: req.query.milestoneId,
                purchase_order_id : req.query.purchaseOrderId,
                milestone_date :req.query.milestoneDate
                
            }
            },
        attributes: ['milestone_id', 'purchase_order_id', 'milestone_date', 'milestone_amount', 'status'],
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'SUCCESS', data: response });
        }
        else {
            res.send({ status: 'SUCCESS', data: [] });
        }
    })
};


//This controller is used for creating new employee.
exports.createNewMilestone= function (req, res) {
    models.Milestone.findOne({
        where: {
            milestone_id: req.body.milestone_id
        }
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'FAILURE', err_msg: 'Milestone is already registered!' });
        } else {
            models.Milestone.create({
                purchase_order_id:req.body.purchase_order_id,
                milestone_name: req.body.milestone_name,
                milestone_date: req.body.milestone_date,
                milestone_amount: req.body.milestone_amount,
                status: req.body.status
            },{
            fields: [
                'purchase_order_id','milestone_name', 'milestone_date', 'milestone_amount', 'status'
            ],
            defaults: {
                created_by: 'system',
                updated_by: 'system',
                created_at: new Date(),
                updated_at: new Date()
            }
        }).then(response => {
            res.send({ status: 'SUCCESS', message: 'New Milestone created successfully .' });
        }).catch(Sequelize.ValidationError, function (err) {
            res.send({ status: 'FAILURE', err_msg: models.sequelizeValidationErrorHandler(err) });
        }).catch(function(err){
            res.send({status : 'FAILURE', err_msg : err});
        });
    }
 })
};



//This controller is used for deleting new allocation.
exports.deleteMilestone = function(req, res) {
    models.Milestone.destroy({
      where: {
        milestone_id: req.params.mileStoneId
      }
    }).then(function() {
        models.Milestone.findAll()
        .then(function (result) {
            res.json({ status: 'SUCCESS', data: result });
        }, function (err) {
            console.log("error occured", err);
            res.json({ status: 'FAILED', msg_code: 'EXP', message: err });
        });
    },function (err) {
        console.log("error occured", err);
        res.json({ status: 'FAILED', msg_code: 'DELETE_ERROR', message: err });
    });
};



