/**
 * @author Mayur Shah
 * 
 * @description This controller is used for perform crud operation also filtering accounts information using required fields.
*/

let models = require('../models');
const Sequelize = models.Sequelize;
const Op = models.Sequelize.Op;

// This controller is used for getting all allocations details.
exports.getAllReportInHtml = function (req, res) {
    models.Allocation.findAll({
        include: [
             {
                model: models.PurchaseOrder,
                attributes : [
                    'po_num', 'po_description', 'type','status'
                ],
                include: [{
                model: models.Customer,
                attributes : [
                    'customer_id', 'first_name', 'last_name','middle_name'
                ],
                include: [{
                    model: models.Account,
                    attributes : [
                        'account_name'
                    ]

                }]
               }]
            },
            {
                model: models.Employee,
                attributes : [
                    'employee_id', 'first_name', 'last_name','middle_name'
                ],
                include: [{
                    model: models.Vendor,
                    attributes : [
                        'company_name'
                    ]

                }]



            }    
        ]
    }).then(function (result) {
        const result_set = result.map(report => {
            return Object.assign(
                {},
                {
                    po_num: report.PurchaseOrder.po_num,
                    po_description: report.PurchaseOrder.po_description,
                    type: report.PurchaseOrder.po_description,
                    status: report.PurchaseOrder.status,
                    amount: report.PurchaseOrder.amount,
                    start_date: report.PurchaseOrder.start_date,
                    end_date: report.PurchaseOrder.end_date,
                    customer_full_name: report.PurchaseOrder.Customer.first_name+" "+report.PurchaseOrder.Customer.last_name,
                    account_name:report.PurchaseOrder.Customer.Account.account_name,
                    employee_full_name:report.Employee.first_name+" "+report.Employee.last_name,
                    role:report.role,
                    employee_location:report.Employee.employee_location,
                    allocation_percentage:report.allocation_percentage,
                    start_date:report.start_date,
                    end_date:report.end_date,
                    status:report.status,
                    rate:report.Employee.rate,
                    vendor_name:report.Employee.Vendor.company_name
                }
            );
        })
        res.json({ status: 'SUCCESS', data: result_set });
    }, function (err) {
        console.log("error occured", err);
        res.json({ status: 'FAILED', msg_code: 'EXP', message: err });
    });
};