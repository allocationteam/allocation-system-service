/**
 * @author Mayur Shah
 * @description This controller is used for perform crud operation.
*/

let models = require('../models');
const Sequelize = models.Sequelize;
const Op = models.Sequelize.Op;

//This controller is used for getting all vendors details without filters.
exports.getAllUsers = function (req, res) {
    models.Vendor.findAll()
        .then(function (result) {
            res.json({ status: 'SUCCESS', data: result });
        }, function (err) {
            console.log("error occured", err);
            res.json({ status: 'FAILED', msg_code: 'EXP', message: err });
        });
};

//This controller is used for getting all employee details with filtering using parameter passing from query string.
exports.getSearchedVendor = function (req, res) {
    models.Vendor.findAll({
        where: {
            $or: {
                 company_name: req.query.companyName,
                 owner_name : req.query.ownerName
                }
            },
        attributes: ['vendor_id', 'company_name', 'owner_name', 'address_line_1', 'address_line_2', 'city', 'country'],
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'SUCCESS', data: response });
        }
        else {
            res.send({ status: 'SUCCESS', data: [] });
        }
    })
};


//This controller is used for creating new employee.
exports.createNewVendor = function (req, res) {
    models.Vendor.findOne({
        where: {
                company_name: req.body.company_name
        }
    }).then(response => {
        if (response !== null) {
            res.send({ status: 'FAILURE', err_msg: 'Vendor is already registered!' });
        } else {
            models.Vendor.create({
            company_name:req.body.company_name,
            owner_name: req.body.owner_name,
            address_line_1: req.body.address_line_1,
            address_line_2: req.body.address_line_2,
            zip_code: req.body.zip_code,
            city: req.body.city,
            country:req.body.country
            },{
            fields: [
                'company_name','owner_name', 'address_line_1', 'address_line_2', 'zip_code', 'city', 'country'
            ],
            defaults: {
                created_by: 'system',
                updated_by: 'system',
                created_at: new Date(),
                updated_at: new Date()
            }
        }).then(response => {
            res.send({ status: 'SUCCESS', message: 'New vendor created successfully .' });
        }).catch(Sequelize.ValidationError, function (err) {
            res.send({ status: 'FAILURE', err_msg: models.sequelizeValidationErrorHandler(err) });
        }).catch(function(err){
            res.send({status : 'FAILURE', err_msg : err});
        });
    }
 })
};

//This controller is used for deleting new account.
exports.deleteVendor = function(req, res) {
    models.Vendor.destroy({
      where: {
        vendor_id: req.params.vendorId
      }
    }).then(function() {
        models.Vendor.findAll()
        .then(function (result) {
            res.json({ status: 'SUCCESS', data: result });
        }, function (err) {
            console.log("error occured", err);
            res.json({ status: 'FAILED', msg_code: 'EXP', message: err });
        });
    },function (err) {
        console.log("error occured", err);
        res.json({ status: 'FAILED', msg_code: 'DELETE_ERROR', message: err });
    });
};



