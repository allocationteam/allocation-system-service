/**
 * @author Ashish Santikari<ashish.santikari@enquero.com>
 * @name : api.js
 * @description : Handles application API
 */ 

let router = require('express').Router();
let customerController = require('../controllers/customer-controller');
let accountController = require('../controllers/account-controller');
let allocationController = require('../controllers/allocation-controller');
let employeeController = require('../controllers/employee-controller');
let milestoneController = require('../controllers/milestone-controller');
let purchaseorderController = require('../controllers/purchaseorder-controller');
let vendorController = require('../controllers/vendor-controller');
let reportController = require('../controllers/report-controller');


/**
 * @method : GET
 * @description : Route to retrive list of all the customers,accounts
 */
//router.get('/customers', customerController.getAllCustomers);
router.get('/allocations', allocationController.getAllAllocations);
router.get('/employees', employeeController.getAllEmployee);
router.get('/milestones', milestoneController.getAllMilestones);
router.get('/purchaseorders', purchaseorderController.getAllPurchaseOrders);
router.get('/vendors', vendorController.getAllVendors);


/**
 * @method : POST
 * @description : Route to add  customer, account
 */
//router.post('/customer/add', customerController.createNewCustomer);
router.post('/allocation/add', allocationController.createNewAllocation);
router.post('/milestone/add', milestoneController.createNewMilestone);
router.post('/employee/add', employeeController.createNewEmployee);
router.post('/purchaseorder/add', purchaseorderController.createNewPurchaseOrder);



/**
 * @method : DELETE
 * @description : Route to add  customer, account
 */
//router.delete('/customer/delete/:customerId', customerController.deleteCustomer);
router.delete('/milestone/delete/:mileStoneId', milestoneController.deleteMilestone);
router.delete('/employee/delete/:employeeId', employeeController.deleteEmployee);
router.delete('/allocation/delete/:allocationId', allocationController.deleteAllocation);
router.delete('/purchaseorder/delete/:purchaseOrderId', purchaseorderController.deletePurchaseOrder);
router.delete('/vendor/delete/:vendorId', vendorController.deleteVendor);

/**
 * @name : /customers/customerid
 * @method : PUT
 * @description : Route to update existing customer using customer id as identifier
 */
router.put('/customer/:customerID', function(req, res){
    res.send('Todo');
});


/**
 * @author Ashish Santikari
 * @description ACCOUNTS API
 */

 /*** get all account list  */  
router.get('/account', accountController.getAllAccounts);

/*** query params accepted are : accountId , accountName, city 
 * Eg. /account/search?accountId=123&accountName=cisco&city=Bengaluru 
 */
router.get('/account/search', accountController.getSearchedAccount);


/**
 * body must contain fields
 * account_name, address_line_1, address_line_2,zip_code,city,country
 */
router.post('/account/add', accountController.createNewAccount);


/**
 * @author Ashish Santikari
 * @description CUSTOMER API
 */

router.get('/customers', customerController.getAllCustomers);
router.post('/customer', customerController.createNewCustomer);

router.get('/employee/search', employeeController.getSearchedEmployee);
router.post('/employee/add', employeeController.createNewEmployee);


router.get('/vendor/search', vendorController.getSearchedVendor);
router.post('/vendor/add', vendorController.createNewVendor);


router.post('/allocation/add', allocationController.createNewAllocation);
router.get('/allocation/search', allocationController.getSearchedAllocation);


router.post('/customer/add', customerController.createNewCustomer);
router.get('/customer/search', customerController.getSearchedCustomer);

router.post('/milestone/add', milestoneController.createNewMilestone);
router.get('/milestone/search', milestoneController.getSearchedMilestone);

//report APIS
//router.get('/report/html/search', reportController.getAllSearchedReportInHtml);
router.get('/report/html', reportController.getAllReportInHtml);




module.exports = router;