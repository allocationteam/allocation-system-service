var express = require('express');
var router = express.Router();
var passport = require('passport')

/* GET home page. */
router.get('/', function(req, res, next) {
  
  console.log(req.user);
  res.render('index', { user: req.user });
});


router.get('/account', ensureAuthenticated, function(req, res){
  res.render('account', { user: req.user });
});

router.get('/login', function(req, res){
  res.render('login', { user: req.user });
});

// GET /auth/outlook
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Outlook authentication will involve
//   redirecting the user to outlook.com.  After authorization, Outlook
//   will redirect the user back to this application at
//   /auth/outlook/callback
router.get('/auth/login',
  passport.authenticate('windowslive', { scope: [
    'openid',
    'profile',
    'offline_access',
    'https://outlook.office.com/Mail.Read'
  ] }),
  function(req, res){
    // The request will be redirected to Outlook for authentication, so
    // this function will not be called.
  });

// GET /auth/outlook/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
router.get('/auth/callback', 
  passport.authenticate('windowslive', { failureRedirect: '/outlook/login' }),
  function(req, res) {
     //sending complete response to UI
     res.send(req.user);

    //res.redirect('/outlook/');
  });

  router.get('/auth/logout', function(req, res){
  req.logout();
  res.redirect('/outlook/');
});

// Simple route middleware to ensure user is authenticated.
//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/outlook/login')
}

module.exports = router;
