'use strict';

let fs        = require('fs');
let path      = require('path');
let Sequelize = require('sequelize');
let basename = path.basename(module.filename);
let config    = require('config');
let db        = {};

let sequelize = new Sequelize(config.get('db_settings'));

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(function(file) {
    let model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.sequelizeValidationErrorHandler = function(err){
  console.log("Err", err);
  const errors_list = err.errors;
  let response_list = [];
  errors_list.forEach(function(error){
      response_list.push({field : error.path , message : error.message});
  });
  return response_list;
}

module.exports = db;
