/**
 * @Author Mayur Shah
* @name : userroles.js
* @description : Model class for table "userRoles"
*/

"use strict";

module.exports = function (sequelize, DataTypes) {
    const UserRoles = sequelize.define('UserRoles',
        {
            user_access_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            user_email_id: {
                type: DataTypes.STRING(100),
                allowNull: false,
                unique: true
            },
            admin_level: {
                type: DataTypes.STRING(1),
                allowNull: true,
                default:'Y'
            },
            po_level: {
                type: DataTypes.STRING(1),
                allowNull: true,
                default:'Y'
            },
            customer_level: {
                type: DataTypes.STRING(1),
                allowNull: true,
                default:'Y'
            },
            account_level: {
                type: DataTypes.STRING(1),
                allowNull: true,
                default:'Y'
            }
        },
        {
            timestamps: true,
            createdAt: 'created_date',
            updatedAt: 'last_updated_date',
            freezeTableName: true
        }
    );
    return UserRoles;
};