/**
* @author : Ashish Santikari<ashish.santikari@enquero.com>
* @name : employee.js
* @description : Model class for table "Employee"
*/

'use strict';

module.exports = function (sequelize, DataTypes) {

    const Employee = sequelize.define('Employee',
        {
            employee_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            employee_number : {
                type : DataTypes.STRING,
                allowNull : false
            },
            salutation: {
                type: DataTypes.STRING(10),
                allowNull: true
            },
            first_name: {
                type: DataTypes.STRING(100),
                allowNull: false
            },
            middle_name: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            last_name: {
                type: DataTypes.STRING(100),
                allowNull: false
            },
            email_id: {
                type: DataTypes.STRING(100),
                allowNull: true,
                validate: {
                    isEmail: true
                }
            },
            phone_number: {
                type: DataTypes.STRING(20),
                allowNull: true
            },
            manager_id: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            designation: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            location: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            status: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            type: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            start_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            end_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            vendor_id: {
                type: DataTypes.INTEGER,
                defaultValue: 0,
                allowNull: false
            },
            created_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            last_updated_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            }
        }, {
            timestamps: true,
            createdAt: 'created_date',
            updatedAt: 'last_updated_date',
            freezeTableName: true
        });

        Employee.associate = function(models){
            Employee.belongsTo(models.Vendor , {foreignKey : 'vendor_id', foreignKeyConstraint : true});
        }

    return Employee;

}