/**
* @author : Ashish Santikari<ashish.santikari@enquero.com>
* @name : customer.js
* @description : Model class for table "Customer"
*/

'use strict';

module.exports = function (sequelize, DataTypes) {

    const Customer = sequelize.define('Customer',
        {
            customer_id: {
                type: DataTypes.INTEGER(11),
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            account_id: {
                type: DataTypes.INTEGER(11),
                allowNull : false
            },
            salutation: {
                type: DataTypes.STRING(10),
                allowNull: true
            },
            first_name: {
                type: DataTypes.STRING(100),
                allowNull: false
            },
            middle_name: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            last_name: {
                type: DataTypes.STRING(100),
                allowNull: false
            },
            designation: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            location: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            contact_number: {
                type: DataTypes.STRING(20),
                allowNull: true,
                isNumeric : {
                    isNum : {
                        msg : 'Contact number must not contain alphabets or symbols.'
                    }
                }
            },
            email_id: {
                type: DataTypes.STRING(100),
                allowNull: true,
                validate: {
                    isEmail: {
                        msg : 'Email ID format is invalid. Please check and retry.'
                    }
                }
            },
            created_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            last_updated_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            }
        },
        {
            timestamps: true,
            createdAt: 'created_date',
            updatedAt: 'last_updated_date',
            freezeTableName: true
        });
    
        Customer.associate = function(models){
            Customer.belongsTo(models.Account, {foreignKey : 'account_id', foreignKeyConstraint : true});
        }

    return Customer;

}