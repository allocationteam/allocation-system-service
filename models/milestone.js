/**
* @author : Ashish Santikari<ashish.santikari@enquero.com>
* @name : milestone.js
* @description : Model class for Table "Milestone"
*/

'use strict';

module.exports = function (sequelize, DataTypes) {

    const Milestone = sequelize.define('Milestone',
        {
            milestone_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            purchase_order_id: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            milestone_name: {
                type: DataTypes.STRING(250),
                allowNull: true
            },
            milestone_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            milestone_amount: {
                type: DataTypes.FLOAT,
                allowNull: true
            },
            status: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            created_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            last_updated_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            }
        }, {
            timestamps: true,
            createdAt: 'created_date',
            updatedAt: 'last_updated_date',
            freezeTableName: true
        }
    );

    return Milestone;

}