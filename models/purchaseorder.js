'use strict';
module.exports = function (sequelize, DataTypes) {

    const PurchaseOrder = sequelize.define('PurchaseOrder',
        {
            purchase_order_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            customer_id: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            po_owner_id: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            po_num: {
                type: DataTypes.STRING(250),
                allowNull: false

            },
            po_description: {
                type: DataTypes.STRING(250),
                allowNull: true
            },
            type: {
                type: DataTypes.STRING(250),
                allowNull: true
            },
            status: {
                type: DataTypes.STRING(250),
                allowNull: true
            },
            amount: {
                type: DataTypes.FLOAT,
                allowNull: true
            },
            start_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            end_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            created_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            last_updated_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
        },
        {
            timestamps: true,
            createdAt: 'created_date',
            updatedAt: 'last_updated_date',
            freezeTableName: true,
        });

        //make all associations here
        PurchaseOrder.associate = function(models){
            PurchaseOrder.belongsTo(models.Customer, { foreignKey: 'customer_id' , foreignKeyConstraint:true });
        }

    return PurchaseOrder;

}