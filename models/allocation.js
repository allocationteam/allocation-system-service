'use strict';
module.exports = function (sequelize, DataTypes) {

    const Allocation = sequelize.define('Allocation',
        {
            allocation_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            purchase_order_id : {
                type : DataTypes.INTEGER,
                allowNull : false                
            },
            employee_id : {
                type : DataTypes.INTEGER,
                allowNull : false
            },
            role: {
                type: DataTypes.STRING(250),
                allowNull: true
            },
            billing_rate: {
                type: DataTypes.FLOAT,
                allowNull: false
            },
            allocation_percent: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            start_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            end_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            created_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            last_updated_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            }
        }, {
            timestamps: true,
            createdAt: 'created_date',
            updatedAt: 'last_updated_date',
            freezeTableName: true
        });


    // Allocation
    Allocation.associate = function(models){
        Allocation.belongsTo(models.PurchaseOrder , {foreignKey : 'purchase_order_id', foreignKeyConstraint : true});
        Allocation.belongsTo(models.Employee , {foreignKey : 'employee_id', foreignKeyConstraint : true});
    };     

    return Allocation;

}