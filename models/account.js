/**
* @author : Ashish Santikari<ashish.santikari@enquero.com>
* @name : account.js
* @description : Model class for table "Account"
*/

"use strict";

module.exports = function (sequelize, DataTypes) {
    const Account = sequelize.define('Account',
        {
            account_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            account_name: {
                type: DataTypes.STRING(100),
                allowNull: false,
                unique: true
            },
            address_line_1: {
                type: DataTypes.STRING(250),
                allowNull: true
            },
            address_line_2: {
                type: DataTypes.STRING(250),
                allowNull: true
            },
            zip_code: {
                type: DataTypes.INTEGER(20),
                allowNull: true,
                validate : {
                    isNumeric : {
                        msg : 'Zip Code should contain only numbers.'
                    }
                }
            },
            city: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            country: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            created_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            },
            last_updated_by: {
                type: DataTypes.STRING(100),
                allowNull: true
            }
        },
        {
            timestamps: true,
            createdAt: 'created_date',
            updatedAt: 'last_updated_date',
            freezeTableName: true
        }
    );
    return Account;
};